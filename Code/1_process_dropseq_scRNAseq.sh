#!/bin/sh
# *******************************************
# Script to process single cell RNAseq data
# *******************************************

release_dir="/mnt/Storage/home/sundongqing/Tools/Pipeline/SCSeq/src/"

# Update with the fullpath location of your sample fastq
fastq_folder="/mnt/Storage/home/sundongqing/Project/Single_cell/Analysis/GSE110823_pub_split-seq/Data/SRR6750042_fastq_split"
fastq_1="SRR6750042_1.fastq"
fastq_2="SRR6750042_2.fastq"
# barcode_lib="$fastq_folder/Barcode_list_2.csv"
outdir="SRR6750042_mm10_split"

#fin="Pt623_DNA031317_b.bam"
#samtools bam2fq $fin > tumor.fastq
#$release_dir/deinterleave_fastq.sh < tumor.fastq tumor_f.fastq tumor_r.fastq

# Parameters related to barcode reads
noLinker_fastq=${fastq_2%.fastq}_noLinker.fastq
barcode_start="0,48,86"
barcode_end="18,56,94"
umi_range=1:10
barcode_range=11:34

# References or annotations(chromosomal names should be compatible.)
#gtf_file="/mnt/Storage/home/zhangwubin/Data_Archive/RefGenome/mm10/mm10_NCBI_RefSeq.gtf"
#fasta_file="/mnt/Storage/home/zhangwubin/Data_Archive/RefGenome/mm10/mm10_Assembly_ucsc_chr.fa"

# bed_file="/mnt/Storage/home/sundongqing/RefGenome/hg38/hg38_gencode.v28.bed"
# ann_file="/mnt/Storage/home/sundongqing/RefGenome/hg38/hg38_gencode_annotation.V28.txt"
# maptool="STAR"
# map_index="/mnt/Storage/home/sundongqing/RefGenome/hg38/STAR_index"

bed_file="/mnt/Storage/home/sundongqing/RefGenome/mm10/mm10_gencode.VM16.bed"
ann_file="/mnt/Storage/home/sundongqing/RefGenome/mm10/mm10_gencode_annotation.VM16.txt"
maptool="STAR"
map_index="/mnt/Storage/home/sundongqing/RefGenome/mm10/STAR_index"
# STAR --runMode genomeGenerate --runThreadN 16 --genomeDir $map_index --genomeFastaFiles $fasta_file --limitGenomeGenerateRAM 42923167786 --sjdbGTFfile $gtf_file

# ******************************************
# 0. Prepare work directory
# ******************************************
#mkdir $outdir
cd $outdir
logfile="run_${outdir}.log"
exec >$logfile 2>&1

# ******************************************
# 1. Remove linker from barcode reads
# ******************************************
echo "1. Remove linker from barcode reads ..."
echo "CMD: python $release_dir/rmLinker.py -f $fastq_folder/$fastq_2 -s $barcode_start -e $barcode_end -o $fastq_folder/$noLinker_fastq"
python $release_dir/rmLinker.py -f $fastq_folder/$fastq_2 -s $barcode_start -e $barcode_end -o $fastq_folder/$noLinker_fastq

# ******************************************
# 2. DrSeq pipeline
# ******************************************
export PYTHONPATH=/mnt/Storage/home/sundongqing/home/DrSeq2/lib/python2.7/site-packages:$PYTHONPATH

echo "2. Run Drseq pipeline ..."
echo "CMD: DrSeq simple -b $fastq_folder/$noLinker_fastq -r $fastq_folder/$fastq_1 --cellbarcoderange $barcode_range --umirange $umi_range -g $ann_file --maptool $maptool --mapindex $map_index --thread 16 --checkmem 1 --select_cell_measure 2 --remove_low_dup_cell 0 -n $outdir -f > ${outdir}_DrSeq.log"
DrSeq simple -b $fastq_folder/$noLinker_fastq -r $fastq_folder/$fastq_1 --cellbarcoderange $barcode_range --umirange $umi_range -g $ann_file --maptool $maptool --mapindex $map_index --thread 16 --checkmem 1 --select_cell_measure 2 --remove_low_dup_cell 0 -n $outdir -f > ${outdir}_DrSeq.log

echo "Plot read distribution ..."
echo "CMD: Rscript $release_dir/ReadDistrView.R $outdir/expmatrix/${outdir}_qcmatfull.txt $outdir/expmatrix/${outdir}_read_distr.png"
Rscript $release_dir/ReadDistrView.R $outdir/expmatrix/${outdir}_qcmatfull.txt $outdir/expmatrix/${outdir}_read_distr.png

# ******************************************
# 3. Run RNASeQC
# ******************************************
echo "3. Run RNASeQC ..."
export PYTHONPATH = /mnt/Storage/home/sundongqing/miniconda2/lib/python2.7/site-packages:$PYTHONPATH
#mkdir RNASeQC
echo "CMD: bam_stat.py -i $outdir/mapping/${outdir}.sam > RNASeQC/${outdir}_bam_stat.txt"
bam_stat.py -i $outdir/mapping/${outdir}.sam > RNASeQC/${outdir}_bam_stat.txt
echo "CMD: read_duplication.py -i $outdir/mapping/${outdir}.sam -o RNASeQC/${outdir}"
read_duplication.py -i $outdir/mapping/${outdir}.sam -o RNASeQC/${outdir}
echo "CMD: read_quality.py -i $outdir/mapping/${outdir}.sam -o RNASeQC/${outdir}"
read_quality.py -i $outdir/mapping/${outdir}.sam -o RNASeQC/${outdir}
echo "CMD: read_distribution.py -i $outdir/mapping/${outdir}.sam -r $bed_file > RNASeQC/${outdir}_read_distribution.txt"
read_distribution.py -i $outdir/mapping/${outdir}.sam -r $bed_file > RNASeQC/${outdir}_read_distr.txt

# *******************************************************************
# 4. Extract reads mapped to each region (intron/intergenic/exon)
# *******************************************************************
# mkdir ReadDist 
# echo "4. Extract reads mapped to each regions ..."
# echo "CMD: python $release_dir/separateReads.py -b $fastq_folder/$noLinker_fastq -r $fastq_folder/$fastq_1 -g $outdir/expmatrix/${outdir}_combined_sort.bed -p ReadDist/$outdir"
# python $release_dir/separateReads.py -b $fastq_folder/$noLinker_fastq -r $fastq_folder/$fastq_1 -g $outdir/expmatrix/${outdir}_combined_sort.bed -p ReadDist/$outdir
