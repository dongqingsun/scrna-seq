#!/bin/sh
# *******************************************
# Script to run velocyto from bam
# *******************************************

# set work dictionary
# work_path="/mnt/Storage/home/sundongqing/Project/Single_cell/Analysis/StanData"
work_path="/mnt/Storage/home/sundongqing/Project/Single_cell/Analysis/GSE110823_pub_split-seq"
output_path="$work_path/Result/velocyto"
sample_name="SRR6750042"
organism="mm10_split"

refgenme_path="/mnt/Storage/home/sundongqing/RefGenome"
mask_file="$refgenme_path/$organism/${organism}_rmsk.gtf"
anno_file="$refgenme_path/$organism/Mus_musculus.GRCm38.92.gtf"
# anno_file="$refgenme_path/$organism/Homo_sapiens.GRCh38.92.gtf"

sam_file="$work_path/Result/scRNAseq_QC/${sample_name}_${organism}/${sample_name}_${organism}/mapping/${sample_name}_${organism}.sam"
bam_file="$work_path/Result/scRNAseq_QC/${sample_name}_${organism}/${sample_name}_${organism}/mapping/${sample_name}_${organism}_barcode.bam"
sorted_bam_file="$work_path/Result/scRNAseq_QC/${sample_name}_${organism}/${sample_name}_${organism}/mapping/${sample_name}_${organism}_barcode_possorted.bam"
barcode_reform_file="$work_path/Result/scRNAseq_QC/${sample_name}_${organism}/${sample_name}_${organism}/expmatrix/${sample_name}_${organism}_barcode_reform.txt"
sam_barcode_outdir="$work_path/Result/scRNAseq_QC/${sample_name}_${organism}/${sample_name}_${organism}/mapping"
cell_sorted_bam_file="$work_path/Result/scRNAseq_QC/${sample_name}_${organism}/${sample_name}_${organism}/mapping/${sample_name}_${organism}_barcode_possorted_cellsorted.bam"
sam_barcode_file="$sam_barcode_outdir/${sample_name}_${organism}_barcode.sam"

# add barcode and umi info into sam file
echo "0. Add barcode and umi info into sam file"
python /mnt/Storage/home/sundongqing/Project/Single_cell/Code/sam_add_barcode_mp.py -s $sam_file -b $barcode_reform_file -o $sam_barcode_outdir -p 8

# activate the environment
source activate py3

# prepare sorted .bam file
echo "1. Prepare sorted .bam file ..."
samtools view $sam_barcode_file -b -o $bam_file --threads 16
samtools sort $bam_file -o $sorted_bam_file --threads 16
# samtools sort $sorted_bam_file -t CB -O BAM -o $cell_sorted_bam_file 

# run velocyto
echo "2. Run velocyto"
velocyto run -o $output_path -m $mask_file $sorted_bam_file $anno_file -e ${sample_name}_${organism}